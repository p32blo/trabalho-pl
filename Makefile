PROJ=PL1314

all: $(PROJ)

install:
	tar -czf $(PROJ).tgz c.l c.y relatorio.tex Makefile

$(PROJ): y.tab.c lex.yy.c
	cc -o $(PROJ) y.tab.c -lm

y.tab.c: c.y
	yacc c.y

lex.yy.c: c.l
	flex c.l

pdf: relatorio.tex
	pdflatex relatorio.tex

clean: 
	rm -f *.aux *.log *.toc *.pdf
	rm -f $(PROJ) lex.yy.c y.tab.c *.tgz
