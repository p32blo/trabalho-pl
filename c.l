%option noyywrap

%%
[0-9]+(.[0-9]+)?	{yylval.d = atof(yytext); return INT;}
[+\n\-/*=()^] 		{return yytext[0];}
[a-z]			{yylval.c = yytext[0]; return ID;}
log			{yylval.f = log10; return FUNC1;}
ln			{yylval.f = log; return FUNC1;}
exp			{yylval.f = exp; return FUNC1;}
.			{fprintf(stderr, "Caracter desconhecido - %c", yytext[0]);}
%%
