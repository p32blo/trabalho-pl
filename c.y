%{
#include <stdio.h>
#include <math.h>

double var[26];

%}

%token INT ID FUNC1

%union {
	char c;
	double d;
	double (*f)(double);
}

%type <c> ID
%type <d> INT exp
%type <f> FUNC1

%right '='
%left '+' '-'
%left '*' '/'
%right '^'

%%

calculadora : calculadora exp '\n'	{printf("%f\n> ", $2);}
	    | calculadora '\n'	 	;
	    |				;

exp : exp '+' exp 	{$$ = $1 + $3;}
    | exp '-' exp  	{$$ = $1 - $3;}
    | exp '*' exp 	{$$ = $1 * $3;}
    | exp '/' exp 	{$$ = $1 / $3;}
    | exp '^' exp 	{$$ = pow($1, $3);}
    | '(' exp ')' 	{$$ = $2;}
    | INT 	  	{$$ = $1;}
    | ID '=' exp 	{$$ = var[$1-'a'] = $3;}
    | ID 		{$$ = var[$1-'a'];}
    | FUNC1 '(' exp ')'	{$$ = (*$1)($3);}
%%

#include "lex.yy.c"

int main() {
	printf("> ");
	yyparse();
	return 0;
}

int yyerror() {
	fprintf(stderr, "Erro Sintatico - \"%s\"", yytext);
	return 0;
}
